package com.example.felipetorres.townpaths.models

import java.io.Serializable


class Point(
    var id: String? = null,
    var parent: String? = null,
    var discovered: Boolean? = false,
    var pointShared: String? = null,
    var image: String? = null,
    var lat: Double? = 0.0,
    var lng: Double? = 0.0,
    var name: String? = null,
    var clueimage: String? = null,
    var cluetext: String? = null,
    var introvideo: String? = null,
    var content: ArrayList<Content>? = null,
    var narrative: ArrayList<Content>? = null) : Serializable