package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Award
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.models.Info
import com.example.felipetorres.townpaths.mvp.view.InfoView
import com.example.felipetorres.townpaths.mvp.view.adapters.AwardsAdapter
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_AWARD
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_DATA
import com.example.felipetorres.townpaths.mvp.view.base.RedeemActivity
import com.example.felipetorres.townpaths.mvp.view.base.getAwardsList
import com.example.felipetorres.townpaths.mvp.view.base.getUserScore
import com.example.felipetorres.townpaths.mvp.view.base.getUserScoreValue
import com.example.felipetorres.townpaths.mvp.view.base.setInfoListData
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener

class InfoPresenter(private val view: InfoView): InfoView.IInfoClicked, AwardsAdapter.IAwardSelected {

    private val context: Activity?
    private var data: Data? = null
    private var points: String? = null
    private var wonAwards: ArrayList<String>? = null
    private lateinit var dialog: Dialog

    init {
        this.context = view.activity
    }

    fun register() {
        val activity = view.activity ?: return
        context?.let {
            getAwardsList(it, object: ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val value = dataSnapshot.value
                    if(value != null){
                        wonAwards = value as ArrayList<String>?
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            }, true)
        }
    }

    fun unregister() {
        val activity = view.activity ?: return

        RxBus.clear(activity)
    }

    fun onStart() {
        view.adapterStartListening()
    }

    fun onStop() {
        view.adapterStopListening()
    }

    override fun goToInfo(info: Info) {
        dialog = Dialog(context!!)
        dialog.setContentView(R.layout.custom_info_dialog)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        dialog.findViewById<TextView>(R.id.name).text = info.name
        dialog.findViewById<TextView>(R.id.description).text = info.description
        Glide.with(dialog.context).load(info.icon).into(dialog.findViewById(R.id.image))
        val list = dialog.findViewById<androidx.recyclerview.widget.RecyclerView>(R.id.list_awards)

        val awardsAvailable = arrayListOf<Award>()
        info.awards?.forEach {
            if (wonAwards == null || wonAwards?.contains(it.id) == false) {
                awardsAvailable.add(it)
        }}

        val adapter = AwardsAdapter(context, awardsAvailable, this, points?.toLong())
        val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        list.layoutManager = mLayoutManager
        list.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        list.adapter = adapter

        if (!view.activity?.isFinishing!!) {
            dialog.show()
        }
    }

    fun getExtras(intent: Intent?) {
        data = intent?.getSerializableExtra(BUNDLE_KEY_DATA) as Data
        getUserScore(data?.id.toString(), view)
        getUserScoreValue(data?.id.toString(), view.activity!!, object: ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.value
                if (value != null) {
                    points = value.toString()
                    setInfoListData(this@InfoPresenter, view, data?.id.toString())
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    override fun onAwardSelected(award: Award) {
        dialog.dismiss()
        val i = Intent(context, RedeemActivity::class.java)
        i.putExtra(BUNDLE_KEY_DATA, data)
        i.putExtra(BUNDLE_KEY_AWARD, award)
        context?.startActivity(i)
    }
}
