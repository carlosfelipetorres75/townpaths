package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.models.Question
import com.example.felipetorres.townpaths.mvp.view.QuizView
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_POINT_DATA
import com.example.felipetorres.townpaths.util.bus.RxBus
import kotlinx.android.synthetic.main.activity_quiz.*

class QuizPresenter(private val view: QuizView) {
    private val context: Activity?
    private lateinit var point: Point
    private var tries = 1

    init {
        this.context = view.activity
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun getExtras(intent: Intent?) {
        point = intent?.getSerializableExtra(BUNDLE_KEY_POINT_DATA) as Point
        val question = point.content?.get(tries) as Question
        context?.cluetext?.text = question.question
        context?.ans1?.text = question.ans1
        context?.ans2?.text = question.ans2
        context?.ans3?.text = question.ans3
    }
}