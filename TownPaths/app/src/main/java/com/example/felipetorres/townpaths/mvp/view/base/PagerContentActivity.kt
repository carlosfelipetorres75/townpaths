package com.example.felipetorres.townpaths.mvp.view.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.PagerContentPresenter
import com.example.felipetorres.townpaths.mvp.view.PagerContentView


class PagerContentActivity: AppCompatActivity() {

    private var presenter: PagerContentPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pager_content)

        presenter = PagerContentPresenter(PagerContentView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }
}
