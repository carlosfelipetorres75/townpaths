package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.User
import com.example.felipetorres.townpaths.mvp.view.LoginView
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_LOGOUT
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_USER
import com.example.felipetorres.townpaths.mvp.view.base.MainActivity
import com.example.felipetorres.townpaths.mvp.view.base.RegisterActivity
import com.example.felipetorres.townpaths.mvp.view.base.SETTINGS_KEY_USER
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.facebook.AccessToken
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.SignInButton
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_login.face_button
import kotlinx.android.synthetic.main.activity_login.google_button
import kotlinx.android.synthetic.main.activity_login.register_button
import org.json.JSONException
import java.util.Arrays


class LoginPresenter(private val view: LoginView) {
    private val context: Activity?
    private val callbackManager: CallbackManager
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var user: User? = null
    private val settings: SharedPreferences

    companion object {
        private val RC_SIGN_IN = 1111
    }

    init {
        this.context = view.activity
        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences

        callbackManager = CallbackManager.Factory.create()
        view.activity?.face_button?.setReadPermissions(Arrays.asList("public_profile", "email"))

        //updateWithToken(AccessToken.getCurrentAccessToken())

        view.activity?.face_button?.registerCallback(callbackManager, object: FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                loginResult.accessToken.userId
                val request = GraphRequest.newMeRequest(
                    loginResult.accessToken) {
                    jsonObject, response ->
                        if (response.error == null) {
                            try {
                                user = User(jsonObject.getString("first_name"),
                                    jsonObject.getString("last_name"),
                                    jsonObject.getString("email"))
                                view.toggleProgressBar()
                                val i = Intent(context, RegisterActivity::class.java)
                                i.putExtra(BUNDLE_KEY_USER, user)
                                context.startActivity(i)
                            } catch (e: JSONException) {
                                view.showMessage("No ha sido posible contectarse con Facebook")
                            }
                        }
                    }
                    val parameters = Bundle()
                    parameters.putString("fields", "id,first_name,last_name,email")
                    request.parameters = parameters
                    request.executeAsync()
            }

            override fun onCancel() {
            }

            override fun onError(e: FacebookException) {
                view.toggleProgressBar()
            }
        })

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = context.let { GoogleSignIn.getClient(it, gso) }

        view.activity?.google_button?.setSize(SignInButton.SIZE_STANDARD)
        view.activity?.google_button?.setOnClickListener {
            val signInIntent = mGoogleSignInClient?.getSignInIntent()
            context.startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        view.activity?.register_button?.setOnClickListener {
            onCallRegisterButtonPressed()
        }
    }

    fun register() {
        val activity = view.activity ?: return
    }

    private fun onCallRegisterButtonPressed() {
        val intent = Intent(context, RegisterActivity::class.java)
        context?.startActivity(intent)
    }

    fun unregister() {
        val activity = view.activity ?: return

        //LoginManager.getInstance().logOut()
        RxBus.clear(activity)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }

    fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            user = User(account?.givenName, account?.familyName, account?.email)
            view.toggleProgressBar()
            val i = Intent(context, RegisterActivity::class.java)
            i.putExtra(BUNDLE_KEY_USER, user)
            context?.startActivity(i)
        } catch (e: ApiException) {
            Log.w("LOGIN_PRESENTER", "signInResult:failed code=" + e.getStatusCode())
            view.showMessage("No ha sido posible contectarse con Google")
        }
    }

    private fun updateWithToken(currentAccessToken: AccessToken?) {
        if (currentAccessToken != null && settings.getString(SETTINGS_KEY_USER, "") != "") {
                val i = Intent(context, MainActivity::class.java)
                context?.startActivity(i)
                context?.finish()
        } else {
            LoginManager.getInstance().logOut()
        }
    }

    fun getExtras(intent: Intent?) {
        val bundle = intent?.getBooleanExtra(BUNDLE_KEY_LOGOUT, false)
        if (bundle == true){
            settings.edit().putString(SETTINGS_KEY_USER, "").apply()
            LoginManager.getInstance().logOut()
            mGoogleSignInClient?.signOut()
        } else {
            val account = GoogleSignIn.getLastSignedInAccount(context)
            if (account?.isExpired == false) {
                user = User(account.givenName, account.familyName, account.email)
                view.toggleProgressBar()
                val i = Intent(context, RegisterActivity::class.java)
                i.putExtra(BUNDLE_KEY_USER, user)
                context?.startActivity(i)
            }
            updateWithToken(AccessToken.getCurrentAccessToken())
        }
    }
}
