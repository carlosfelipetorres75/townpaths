package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.view.View
import android.widget.RadioButton
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.User
import com.example.felipetorres.townpaths.mvp.view.RegisterView
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_USER
import com.example.felipetorres.townpaths.mvp.view.base.MainActivity
import com.example.felipetorres.townpaths.mvp.view.base.isUserRegistered
import com.example.felipetorres.townpaths.mvp.view.base.setUserInfo
import com.example.felipetorres.townpaths.util.bus.RxBus
import kotlinx.android.synthetic.main.activity_register.*

class RegisterPresenter(private val view: RegisterView) {
    private val context: Activity?
    private val settings: SharedPreferences

    init {
        this.context = view.activity
        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences
        view.activity?.btn_register?.setOnClickListener {
            val selectedId = context.radioPlace?.checkedRadioButtonId
            if (selectedId == null ||
                context.et_names?.text?.isEmpty() == true ||
                context.et_surnames?.text?.isEmpty() == true ||
                context.et_username?.text?.isEmpty() == true ||
                context.et_age?.text?.isEmpty() == true) {
                view.showMessage("Completa la informacion antes de continuar")
                return@setOnClickListener
            }
            val radioPlaceButton = view.activity?.findViewById(selectedId) as RadioButton

            val user = User(context.et_names?.text.toString(),
                context.et_surnames?.text.toString(),
                context.et_username?.text.toString(),
                radioPlaceButton.text.toString(),
                context.et_age?.text.toString())
            view.toggleProgressBar()

            setUserInfo(context, user)
        }
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun getExtras(intent: Intent?) {
        val user = intent?.getSerializableExtra(BUNDLE_KEY_USER)
        user?.let { it as User
            context?.et_names?.setText(it.names)
            context?.et_surnames?.setText(it.surnames)
            context?.et_username?.setText(it.email)
            it.email?.let { it1 ->
                if (context != null) {
                    view.toggleProgressBar()
                    context.form_register.visibility = View.GONE
                    isUserRegistered(view, context, it1, {
                        val i = Intent(context, MainActivity::class.java)
                        context.startActivity(i)
                        context.finishAffinity()
                    }, {
                        context.form_register.visibility = View.VISIBLE
                    })
                }
            }
        }

    }
}