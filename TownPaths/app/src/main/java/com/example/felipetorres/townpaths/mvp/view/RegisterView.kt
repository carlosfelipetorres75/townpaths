package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AppCompatActivity

class RegisterView(activity: AppCompatActivity): ActivityView(activity){
    init {
        setTitleToolbar("Registrate")
    }
}
