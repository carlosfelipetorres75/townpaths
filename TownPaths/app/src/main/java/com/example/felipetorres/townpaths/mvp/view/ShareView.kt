package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.models.User
import com.example.felipetorres.townpaths.mvp.view.adapters.UserViewHolder
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.activity_share.*

class ShareView(activity: AppCompatActivity): ActivityView(activity){

    private var adapter: FirebaseRecyclerAdapter<Data, UserViewHolder>? = null

    init {
        setTitleToolbar("¡Comparte!")
    }

    fun setUsersList(options: FirebaseRecyclerOptions<Data>, pointData: Point?) {
        val mLayoutManager = androidx.recyclerview.widget.GridLayoutManager(context, 3)
        mLayoutManager.reverseLayout = true
        activity?.share_list?.layoutManager = mLayoutManager
        activity?.share_list?.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()

        adapter = object: FirebaseRecyclerAdapter<Data, UserViewHolder>(options) {

            override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): UserViewHolder {
                val inflater = LayoutInflater.from(viewGroup.context)
                return UserViewHolder(inflater.inflate(R.layout.card_view_user, viewGroup, false))
            }

            override fun onBindViewHolder(viewHolder: UserViewHolder, position: Int, model: Data) {
                model.pointlist?.get(pointData?.id.toString())
                    ?.pointShared?.let { viewHolder.bindToUser(it) }
            }
        }
        activity?.share_list?.adapter = adapter
    }

    fun adapterStartListening() {
        if (adapter != null) {
            adapter?.startListening()
        }
    }

    fun adapterStopListening() {
        if (adapter != null) {
            adapter?.stopListening()
        }
    }
}
