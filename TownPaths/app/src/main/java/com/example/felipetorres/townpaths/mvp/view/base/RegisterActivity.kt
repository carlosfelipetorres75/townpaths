package com.example.felipetorres.townpaths.mvp.view.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.RegisterPresenter
import com.example.felipetorres.townpaths.mvp.view.RegisterView

class RegisterActivity: AppCompatActivity() {

    private var presenter: RegisterPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_register)

        presenter = RegisterPresenter(RegisterView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }
}
