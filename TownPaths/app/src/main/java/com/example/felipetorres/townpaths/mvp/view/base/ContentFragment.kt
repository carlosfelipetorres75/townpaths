package com.example.felipetorres.townpaths.mvp.view.base

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Content
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.adapters.ContentAdapter
import kotlinx.android.synthetic.main.fragment_content.*

class ContentFragment : Fragment(), ContentAdapter.ISelectedContent{

    private var pointData: Point? = null
    private var adapter: ContentAdapter? = null
    private var page: Int = 0

    companion object {
        fun newInstance(pointData: Point, page: Int): ContentFragment {
            val contentFragment = ContentFragment()
            val args = Bundle()
            args.putSerializable(BUNDLE_KEY_POINT_DATA, pointData)
            args.putInt(BUNDLE_KEY_PAGE, page)
            contentFragment.arguments = args
            return contentFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        page = arguments?.getInt(BUNDLE_KEY_PAGE, 0)!!
        pointData = arguments?.getSerializable(BUNDLE_KEY_POINT_DATA) as Point
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val view = inflater.inflate(R.layout.fragment_content, container, false)
        val contentList = view.findViewById(R.id.content_list) as androidx.recyclerview.widget.RecyclerView
        setDataList(contentList)
        return view
    }

    private fun setDataList(contentList: androidx.recyclerview.widget.RecyclerView) {
        val content = if (page == 0) pointData?.content?.filterNotNull() else pointData?.narrative?.filterNotNull()
        if (content.isNullOrEmpty()) return
        adapter = ContentAdapter(content, this)
        val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        contentList.layoutManager = mLayoutManager
        contentList.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()
        contentList.adapter = adapter
    }

    override fun goToSelectedContent(content: Content?, pos: Int) {
        val i = Intent(context, ContentDetailActivity::class.java)
        i.putExtra(BUNDLE_KEY_POSITION, pos)
        i.putExtra(BUNDLE_KEY_CONTENT, content)
        i.putExtra(BUNDLE_KEY_POINT_DATA, pointData)
        i.putExtra(BUNDLE_KEY_CONTENT_TYPE, if (page == 0) "content" else "narrative")
        context?.startActivity(i)
    }
}
