package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ContentDetailView(activity: AppCompatActivity): ActivityView(activity){
    init {
        setTitleToolbar("Más contenido")

        backArrow?.setOnClickListener {
            val builder = AlertDialog.Builder(activity)
            builder.setMessage("Si sales del video puedes perder puntos")
            builder.setCancelable(true)

            builder.setPositiveButton("Salir") {
                dialog, _ ->  dialog.cancel()
                activity.onBackPressed()
            }

            builder.setNegativeButton("Continuar"){ dialog, _ -> dialog.cancel() }

            val alert = builder.create()
            alert.show()
        }
    }
}
