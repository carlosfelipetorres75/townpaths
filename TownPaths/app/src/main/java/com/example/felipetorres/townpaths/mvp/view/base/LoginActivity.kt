package com.example.felipetorres.townpaths.mvp.view.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.LoginPresenter
import com.example.felipetorres.townpaths.mvp.view.LoginView

class LoginActivity: AppCompatActivity() {

    private var presenter: LoginPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        presenter = LoginPresenter(LoginView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        presenter?.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data);
    }
}
