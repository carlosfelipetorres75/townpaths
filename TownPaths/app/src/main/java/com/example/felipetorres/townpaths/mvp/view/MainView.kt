package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.mvp.view.adapters.DataViewHolder
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.activity_main.rv_data

class MainView(activity: AppCompatActivity): ActivityView(activity) {

    private val iDataClicked: IDataClicked? = null

    private var adapter: FirebaseRecyclerAdapter<Data, DataViewHolder>? = null

    fun setDataList(options: FirebaseRecyclerOptions<Data>, iDataClicked: IDataClicked) {

        toggleProgressBar()
        val mLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(context)
        mLayoutManager.reverseLayout = true
        mLayoutManager.stackFromEnd = true
        activity?.rv_data?.layoutManager = mLayoutManager
        activity?.rv_data?.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()

        adapter = object: FirebaseRecyclerAdapter<Data, DataViewHolder>(options) {

            override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): DataViewHolder {
                val inflater = LayoutInflater.from(viewGroup.context)
                toggleProgressBar()
                return DataViewHolder(inflater.inflate(R.layout.card_view, viewGroup, false))
            }

            override fun onBindViewHolder(viewHolder: DataViewHolder, position: Int, model: Data) {
                viewHolder.bindToData(model, View.OnClickListener {
                    iDataClicked.goToData(model)
                })
            }

//            override fun onDataChanged() {
//                if (adapter?.itemCount == 1) {
//                    adapter?.getItem(0)?.let { iDataClicked.goToData(it) }
//                }
//                super.onDataChanged()
//            }
        }
        activity?.rv_data?.adapter = adapter
    }

    fun adapterStartListening() {
        if (adapter != null) {
            adapter?.startListening()
        }
    }

    fun adapterStopListening() {
        if (adapter != null) {
            adapter?.stopListening()
        }
    }

    interface IDataClicked {
        fun goToData(data: Data)
    }
}
