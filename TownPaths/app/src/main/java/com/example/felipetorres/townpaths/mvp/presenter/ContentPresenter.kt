package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.view.View.VISIBLE
import android.widget.MediaController
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.ContentView
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_POINT_DATA
import com.example.felipetorres.townpaths.mvp.view.base.PagerContentActivity
import com.example.felipetorres.townpaths.mvp.view.base.SCORE_MEDIUM
import com.example.felipetorres.townpaths.mvp.view.base.getUserScore
import com.example.felipetorres.townpaths.mvp.view.base.setUserScore
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_content.*


class ContentPresenter(val view: ContentView) {
    private val context: Activity?
    private var mediaControls: MediaController? = null
    private var position = 0
    private var progressDialog: ProgressDialog? = null
    private var pointData: Point? = null
    private val settings: SharedPreferences

    init {
        this.context = view.activity
        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun getExtras(intent: Intent?) {
        pointData = intent?.getSerializableExtra(BUNDLE_KEY_POINT_DATA) as Point
        setVideoSettings()
        context?.button_next?.setOnClickListener {
            val i = Intent(context, PagerContentActivity::class.java)
            i.putExtra(BUNDLE_KEY_POINT_DATA, pointData)
            context.finish()
            context.startActivity(i)
        }
        getUserScore(pointData?.parent.toString(), view)
    }

    private fun setVideoSettings() {
        if (mediaControls == null) {
            mediaControls = MediaController(context)
        }

        progressDialog = ProgressDialog(context)
        progressDialog?.setTitle("Un Momento")
        progressDialog?.setMessage("Cargando video de " + pointData?.name)
        progressDialog?.setCancelable(false)
        progressDialog?.show()

        //context?.video?.setMediaController(mediaControls)
        context?.video?.setVideoURI(Uri.parse(pointData?.introvideo))
        context?.video?.requestFocus()
        context?.video?.setOnPreparedListener {
            progressDialog?.dismiss()
            context.video?.seekTo(position)
            if (position == 0) {
                context.video?.start()
            } else {
                context.video?.pause()
            }
            Thread {
                try {
                    val duration = context.video.duration
                    context.progress_bar.max = duration
                    do {
                        val time = (duration - context.video.currentPosition) / 1000
                        context.runOnUiThread { if (time < 30) context.button_next.visibility = VISIBLE }
                        context.progress_bar.progress = context.video.currentPosition
                    } while (context.video.currentPosition < duration)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }.start()
        }

        context?.video?.setOnCompletionListener {
            context.button_next.visibility = VISIBLE
            setUserScore(pointData?.parent.toString(), SCORE_MEDIUM, context, true, pointData?.id + "introvideo") {
                Snackbar.make(context.video, if (it) "Puntos acumulados" else "No puedes acumular dos veces", Snackbar.LENGTH_LONG).show()
            }
        }
        context?.video?.setOnErrorListener { mediaPlayer, i, j ->
            mediaPlayer.stop()
            false
        }
    }
}