package com.example.felipetorres.townpaths.mvp.view.adapters

import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Info


class InfoViewHolder(val view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    private val name: TextView = view.findViewById(R.id.tv_name)
    private val card: androidx.cardview.widget.CardView = view.findViewById(R.id.cv_image_info)
    private val image: ImageView = view.findViewById(R.id.iv_town)

    fun bindToInfo(info: Info, startClickListener: View.OnClickListener) {
        name.text = info.name
        Glide.with(view.context).load(info.icon).into(image)
        card.setOnClickListener(startClickListener)
    }

}