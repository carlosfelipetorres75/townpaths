package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Info
import com.example.felipetorres.townpaths.mvp.view.adapters.InfoViewHolder
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import kotlinx.android.synthetic.main.activity_info.*

class InfoView(activity: AppCompatActivity): ActivityView(activity) {

    private var adapter: FirebaseRecyclerAdapter<Info, InfoViewHolder>? = null

    init {
        setTitleToolbar("Recompensas")
    }

    fun setInfoList(options: FirebaseRecyclerOptions<Info>, iInfoClicked: IInfoClicked) {

        val mLayoutManager = GridLayoutManager(context, 2)
        mLayoutManager.reverseLayout = true
        activity?.rv_info?.layoutManager = mLayoutManager
        activity?.rv_info?.itemAnimator = androidx.recyclerview.widget.DefaultItemAnimator()

        adapter = object: FirebaseRecyclerAdapter<Info, InfoViewHolder>(options) {

            override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): InfoViewHolder {
                val inflater = LayoutInflater.from(viewGroup.context)
                return InfoViewHolder(inflater.inflate(R.layout.card_view_info, viewGroup, false))
            }

            override fun onBindViewHolder(viewHolder: InfoViewHolder, position: Int, model: Info) {
                viewHolder.bindToInfo(model, View.OnClickListener {
                    iInfoClicked.goToInfo(model)
                })
            }
        }
        activity?.rv_info?.adapter = adapter
        adapter?.startListening()
    }

    fun adapterStartListening() {
        if (adapter != null) {
            adapter?.startListening()
        }
    }

    fun adapterStopListening() {
        if (adapter != null) {
            adapter?.stopListening()
        }
    }

    interface IInfoClicked {
        fun goToInfo(info: Info)
    }
}
