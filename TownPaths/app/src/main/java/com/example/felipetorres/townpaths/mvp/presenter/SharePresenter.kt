package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.ShareView
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_POINT_DATA
import com.example.felipetorres.townpaths.mvp.view.base.getUserScore
import com.example.felipetorres.townpaths.mvp.view.base.setUsersListData
import com.example.felipetorres.townpaths.mvp.view.base.uploadPointImage
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.share.Sharer
import com.facebook.share.model.ShareHashtag
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.model.SharePhoto
import com.facebook.share.model.SharePhotoContent
import kotlinx.android.synthetic.main.activity_share.fb_share_button
import kotlinx.android.synthetic.main.activity_share.fb_share_button2
import kotlinx.android.synthetic.main.activity_share.takedPhoto
import java.io.File


class SharePresenter(private val view: ShareView) {

    private val context: Activity?
    private var imageUri: Uri? = null
    private val TAKE_PICTURE = 1
    private var pointData: Point? = null
    private var fbManager: CallbackManager? = null

    private val settings: SharedPreferences

    init {
        this.context = view.activity

        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences

        val content = ShareLinkContent.Builder()
            .setQuote("Mongui mongui mongui, mi tierrita preferida...")
            .setShareHashtag(ShareHashtag.Builder().setHashtag("#mongui").build())
            .setContentUrl(Uri.parse("http://www.colombia.travel/en/where-to-go/andean/mongui"))
            .build()

        context.fb_share_button?.shareContent = content
        fbManager = CallbackManager.Factory.create()
        context.fb_share_button2?.registerCallback(fbManager, object: FacebookCallback<Sharer.Result> {
            override fun onSuccess(result: Sharer.Result) {
                Toast.makeText(context, "You shared this post", Toast.LENGTH_SHORT).show()
                uploadImage()
            }

            override fun onCancel() {
            }

            override fun onError(e: FacebookException) {
                e.printStackTrace()
            }
        })
    }

    fun register() {
        val activity = view.activity ?: return
    }

    fun unregister() {
        val activity = view.activity ?: return

        RxBus.clear(activity)
    }

    fun onStart() {
        view.adapterStartListening()
    }

    fun onStop() {
        view.adapterStopListening()
    }

    fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val photo = File(Environment.getExternalStorageDirectory(), "Pic.jpg")
        if(!photo.exists()) photo.mkdirs()
        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photo))
        imageUri = Uri.fromFile(photo)
        context?.startActivityForResult(intent, TAKE_PICTURE)
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        fbManager?.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            TAKE_PICTURE ->
                if (resultCode == Activity.RESULT_OK) {
                    context?.contentResolver?.notifyChange(imageUri!!, null)
                    try {
                        val rotatedBitmap = rotateImage()

                        context?.takedPhoto?.setImageBitmap(rotatedBitmap)

                        val photo = SharePhoto.Builder()
                            .setBitmap(rotatedBitmap)
                            .build()

                        val content = SharePhotoContent.Builder()
                            .addPhoto(photo)
                            .build()

                        context?.fb_share_button2?.shareContent = content
                    } catch (e: Exception) {
                        Log.e("Camera", e.toString())
                    }
                }
        }
    }

    private fun rotateImage(): Bitmap? {
        val bounds = BitmapFactory.Options()
        bounds.inJustDecodeBounds = true
        BitmapFactory.decodeFile(imageUri?.path, bounds)

        val opts = BitmapFactory.Options()
        val bm = BitmapFactory.decodeFile(imageUri?.path, opts)
        val exif = ExifInterface(imageUri?.path)
        val orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION)
        val orientation = if (orientString != null) Integer.parseInt(
            orientString) else ExifInterface.ORIENTATION_NORMAL

        var rotationAngle = 0
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270

        val matrix = Matrix()
        matrix.setRotate(rotationAngle.toFloat(), bm.width.toFloat() / 2, bm.height.toFloat() / 2)
        return Bitmap.createBitmap(bm, 0, 0, bounds.outWidth,
            bounds.outHeight, matrix, true)
    }

    fun getExtras(intent: Intent?) {
        pointData = intent?.getSerializableExtra(BUNDLE_KEY_POINT_DATA) as Point
        setUsersListData(view, pointData)
        getUserScore(pointData?.parent.toString(), view)
    }

    private fun uploadImage() {
        if (imageUri != null) {
            uploadPointImage(pointData?.id.toString(), pointData?.parent.toString(), context!!, imageUri)
        }
    }
}
