package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import androidx.appcompat.app.AlertDialog
import android.util.Log
import android.view.View.GONE
import android.view.View.VISIBLE
import com.bumptech.glide.Glide
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.ClueView
import com.example.felipetorres.townpaths.mvp.view.base.*
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import kotlinx.android.synthetic.main.activity_clue.button_next
import kotlinx.android.synthetic.main.activity_clue.clueimage
import kotlinx.android.synthetic.main.activity_clue.cluetext
import kotlinx.android.synthetic.main.activity_clue.map_help

class CluePresenter(val view: ClueView) : GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private var mGoogleApiClient: GoogleApiClient? = null
    private var location: Location? = null
    private lateinit var mLocationRequest: LocationRequest
    private val settings: SharedPreferences

    private val context: Activity?
    private var alreadyStarted = false
    private var pointData: Point? = null
    private var data: Data? = null

    init {
        this.context = view.activity
        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences
        mGoogleApiClient = context.let {
            GoogleApiClient.Builder(it)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        }

        context.map_help?.setOnClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setMessage("Si continúas con la ayuda perderás puntos")
            builder.setCancelable(true)

            builder.setPositiveButton("Continuar") {
                dialog, _ ->  dialog.cancel()
                val i = Intent(context, MapActivity::class.java)
                i.putExtra(BUNDLE_KEY_DATA, data)
                i.putExtra(BUNDLE_KEY_POINT_DATA, pointData)
                i.putExtra(BUNDLE_KEY_NORMAL_MAP, true)
                context.startActivity(i)
            }

            builder.setNegativeButton("Atrás"){ dialog, _ -> dialog.cancel() }

            val alert = builder.create()
            alert.show()
        }
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun onStart() {
        mGoogleApiClient?.connect()
    }

    fun onStop() {
        mGoogleApiClient?.disconnect()
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 1000 // Update location every 1 seconds

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)

        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (location == null) {
            context?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            return
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i(MapActivity::class.java.name, "GoogleApiClient connection has been suspend")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i(MapActivity::class.java.name, "GoogleApiClient connection has failed")
    }

    override fun onLocationChanged(location: Location?) {
        Log.i(MapActivity::class.java.name, "Location received: " + location.toString())
        if (distance(pointData?.lat, pointData?.lng, location?.latitude, location?.longitude, "K") < 0.1){
            if (!alreadyStarted) {
                goToContent()
                alreadyStarted = true
                context?.button_next?.setOnClickListener { goToContent() }
            }
            context?.button_next?.visibility = VISIBLE
        } else {
            context?.button_next?.visibility = GONE
        }
        //view.showMessage("distancia: " + distance(pointData?.lat, pointData?.lng, location?.latitude, location?.longitude, "K"))
    }

    private fun goToContent() {
        if (context != null) {
            setUserScore(data?.id.toString(), SCORE_MEDIUM, context, true)
            setDiscoveredPointValue(data?.id.toString(), pointData?.id.toString(), true, context)
        }

        Handler().postDelayed({
            val i = Intent(context, ContentActivity::class.java)
            i.putExtra(BUNDLE_KEY_POINT_DATA, pointData)
            context?.finish()
            context?.startActivity(i)
        }, 4000)
    }

    private fun distance(lat1: Double?, lon1: Double?, lat2: Double?, lon2: Double?, unit: String): Double {
        val lat1D = lat1 ?: 0.0
        val lon1D = lon1 ?: 0.0
        val lat2D = lat2 ?: 0.0
        val lon2D = lon2 ?: 0.0
        val theta = lon1D - lon2D
        var dist = Math.sin(deg2rad(lat1D)) * Math.sin(deg2rad(lat2D)) + Math.cos(
            deg2rad(lat1D)) * Math.cos(deg2rad(lat2D)) * Math.cos(deg2rad(theta))
        dist = Math.acos(dist)
        dist = rad2deg(dist)
        dist *= 60.0 * 1.1515
        if (unit === "K") {
            dist *= 1.609344
        } else if (unit === "N") {
            dist *= 0.8684
        }

        return dist
    }

    private fun deg2rad(deg: Double) = deg * Math.PI / 180.0

    private fun rad2deg(rad: Double) = rad * 180 / Math.PI

    fun getExtras(intent: Intent?) {
        pointData = intent?.getSerializableExtra(BUNDLE_KEY_POINT_DATA) as Point
        data = intent.getSerializableExtra(BUNDLE_KEY_DATA) as Data
        setClueInformation()
        getUserScore(data?.id.toString(), view)

        val inMongui = settings.getBoolean(SETTINGS_KEY_IN_MOGUI, false)
        if (!inMongui) {
            goToContent()
        }
    }

    private fun setClueInformation() {
        context?.cluetext?.text = pointData?.cluetext
        if (context != null) Glide.with(context).load(pointData?.clueimage).into(context.clueimage)
    }
}