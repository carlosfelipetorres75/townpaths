package com.example.felipetorres.townpaths.mvp.view

import android.content.Context
import android.graphics.Color
import com.google.android.material.snackbar.Snackbar
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.WindowManager
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.example.felipetorres.townpaths.R
import java.lang.ref.WeakReference

abstract class ActivityView(activity: AppCompatActivity) {

    internal var progressBar: ProgressBar? = null
    internal var score: TextView? = null
    internal var titleToolbar: TextView? = null
    internal var backArrow: ImageView? = null

    private val activityRef: WeakReference<AppCompatActivity> = WeakReference(activity)

    val activity: AppCompatActivity?
        get() = activityRef.get()

    val context: Context?
        get() = activity

    init {
        progressBar = activity.findViewById(R.id.progress_bar)
        titleToolbar = activity.findViewById(R.id.title_toolbar)
        score = activity.findViewById(R.id.points)
        backArrow = activity.findViewById(R.id.back_arrow)
        backArrow?.setOnClickListener { activity.onBackPressed() }
    }

    fun toggleProgressBar() {
        if (progressBar?.visibility == View.INVISIBLE) {
            progressBar?.visibility = View.VISIBLE
            activity?.window?.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            progressBar?.visibility = View.INVISIBLE
            activity?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    fun showMessage(text: String?, error: Boolean = false) {
        var message = text
        if (message == null || message.isEmpty()) message = "Algo salio mal.."
        if (activity?.isFinishing == false) {
            val snackbar = this.progressBar?.let { Snackbar.make(it, message, Snackbar.LENGTH_LONG) }
            val sbView = snackbar?.view
            val color = if(error) R.color.colorPrimary else R.color.colorAccent
            sbView?.setBackgroundColor(ContextCompat.getColor(activity!!, color))
            val textView = sbView?.findViewById<TextView>(R.id.snackbar_text)
            textView?.setTextColor(Color.WHITE)
            snackbar?.show()
        }
    }

    fun setTitleToolbar(title: String?) {
        if (titleToolbar != null) {
            titleToolbar?.setText(title)
        }
    }

    fun setScoreValue(scoreText: String?) {
        if (score != null) {
            score?.text = scoreText
        }
    }
}
