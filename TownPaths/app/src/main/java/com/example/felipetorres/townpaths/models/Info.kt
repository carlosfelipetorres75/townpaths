package com.example.felipetorres.townpaths.models

import java.io.Serializable


class Info(
    var name: String? = null,
    var idTown: String? = null,
    var description: String? = null,
    var icon: String? = null,
    var lat: Double? = 0.0,
    var lng: Double? = 0.0,
    var awards: ArrayList<Award>? = null): Serializable