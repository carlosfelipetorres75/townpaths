package com.example.felipetorres.townpaths.util.bus.observers

abstract class CallRegisterButtonObserver: BusObserver<CallRegisterButtonObserver.CallRegisterButtonPressed>(
    CallRegisterButtonPressed::class.java) {

    class CallRegisterButtonPressed
}