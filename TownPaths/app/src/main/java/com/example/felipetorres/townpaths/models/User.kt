package com.example.felipetorres.townpaths.models

import java.io.Serializable

class User(
    var names: String? = null,
    var surnames: String? = null,
    var email: String? = null,
    var place: String? = null,
    var age: String? = null,
    var towns: HashMap<String, Data>? = null,
    var awards: ArrayList<String>? = null): Serializable
