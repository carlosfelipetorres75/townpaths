package com.example.felipetorres.townpaths.mvp.view.base

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.widget.Toast
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.models.Info
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.models.User
import com.example.felipetorres.townpaths.mvp.view.ActivityView
import com.example.felipetorres.townpaths.mvp.view.InfoView
import com.example.felipetorres.townpaths.mvp.view.MainView
import com.example.felipetorres.townpaths.mvp.view.ShareView
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import java.util.HashMap


private val mDatabase = FirebaseDatabase.getInstance().reference
private val mStorage = FirebaseStorage.getInstance().reference

fun getCurrentUser(context: Context): String {
    val settings = context.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences
    return settings.getString(SETTINGS_KEY_USER, "") as String
}

fun setCurrentUser(context: Context, currentUser: String) {
    val settings = context.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences
    settings.edit().putString(SETTINGS_KEY_USER, currentUser).apply()
}

fun getUserScore(dataId: String, view: ActivityView) {
    val user = view.activity?.let { getCurrentUser(it) }
    mDatabase
            .child(USER_KEY_USERS)
            .child(user.toString())
            .child(TOWNS_KEY_TOWNS)
            .child(dataId)
            .child(USER_KEY_SCORE)
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    val value = dataSnapshot.value
                    if (value == null) {
                        val usersRef = mDatabase
                                .child(USER_KEY_USERS)
                                .child(user.toString())
                                .child(TOWNS_KEY_TOWNS)
                                .child(dataId)
                                .child(USER_KEY_SCORE)
                        usersRef.setValue(0)
                        view.setScoreValue("Puntos: 0")
                    } else {
                        view.setScoreValue("Puntos: $value")
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
}

fun getUserScoreValue(dataId: String, context: Context, listener: ValueEventListener) {
    val user = getCurrentUser(context)
    mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(TOWNS_KEY_TOWNS)
            .child(dataId)
            .child(USER_KEY_SCORE)
            .addValueEventListener(listener)
}

fun setUserScore(dataId: String, wonScore: Long, context: Context, add: Boolean, contentId: String? = null, callback: (Boolean) -> Unit?) {
    if (contentId == null) {
        setUserScore(dataId, wonScore, context, add)
    } else {
        getSeenContent(context, object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {}

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.value
                var seenContent = hashMapOf<String, String>()
                if (value != null) {
                    seenContent = value as HashMap<String, String>
                }
                if (!seenContent.containsValue(contentId) && add) {
                    setUserScore(dataId, wonScore, context, add)
                    seenContent.put(contentId, contentId)
                    setSeenContent(seenContent, context)
                    callback(true)
                } else {
                    callback(false)
                }
            }

        })
    }
}

fun setUserScore(dataId: String, wonScore: Long, context: Context, add: Boolean) {
    val user = getCurrentUser(context)
    val scoreRef = mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(TOWNS_KEY_TOWNS)
            .child(dataId)
            .child(USER_KEY_SCORE)
    scoreRef.keepSynced(false)
    scoreRef.addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val value = dataSnapshot.value
            if (value != null) {
                val newScore = if (add) (value as Long).plus(wonScore) else (value as Long).minus(wonScore)
                scoreRef.setValue(newScore)
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {

        }
    })
}

fun setSeenContent(contentList: HashMap<String, String>, context: Context) {
    val user = getCurrentUser(context)
    mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(USER_KEY_SEEN_CONTENT)
            .setValue(contentList)
}

fun getSeenContent(context: Context, listener: ValueEventListener) {
    val user = getCurrentUser(context)
    mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(USER_KEY_SEEN_CONTENT)
            .addListenerForSingleValueEvent(listener)
}

fun isUserRegistered(view: ActivityView, context: Activity, userEmail: String, callback: () -> Unit, callbackNotFound: () -> Unit) {
    val userEmailReplaced = userEmail.replace(".", "")
    mDatabase
            .child(USER_KEY_USERS)
            .child(userEmailReplaced)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    view.toggleProgressBar()
                    val value = dataSnapshot.value
                    if (value != null) {
                        callback()
                        setCurrentUser(context, userEmailReplaced)
                    } else {
                        callbackNotFound()
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
}

fun getAwardsList(context: Activity, listener: ValueEventListener, sycn: Boolean) {
    val user = getCurrentUser(context)
    val ref = mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(USER_KEY_AWARDS)
    if (sycn) ref.addValueEventListener(listener)
    else ref.addListenerForSingleValueEvent(listener)
}

fun setWonAwards(context: Activity, awardId: String) {
    val user = getCurrentUser(context)
    getAwardsList(context, object : ValueEventListener {
        override fun onCancelled(p0: DatabaseError) {
        }

        override fun onDataChange(dataSnapshot: DataSnapshot) {
            val value = dataSnapshot.value
            var wonAwards = arrayListOf<String>()
            if (value != null) {
                wonAwards = value as ArrayList<String>
            }
            wonAwards.add(awardId)
            mDatabase
                    .child(USER_KEY_USERS)
                    .child(user)
                    .child(USER_KEY_AWARDS).setValue(wonAwards)
        }

    }, false)
}

fun setUserInfo(context: Activity, user: User) {
    val userEmailReplaced = user.email?.replace(".", "")
    userEmailReplaced?.let { setCurrentUser(context, it) }

    if (userEmailReplaced != null) {
        mDatabase
                .child(USER_KEY_USERS)
                .child(userEmailReplaced)
                .addValueEventListener(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        val value = dataSnapshot.value
                        if (value == null) {
                            val usersRef = mDatabase
                                    .child(USER_KEY_USERS)
                                    .child(userEmailReplaced)
                            usersRef.setValue(user)
                        }
                        val i = Intent(context, MainActivity::class.java)
                        context.startActivity(i)
                        context.finishAffinity()
                    }

                    override fun onCancelled(databaseError: DatabaseError) {

                    }
                })
    }
}

fun setDiscoveredPointValue(dataId: String, pointId: String, value: Boolean, context: Context) {
    val user = getCurrentUser(context)
    val usersRef = mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(TOWNS_KEY_TOWNS)
            .child(dataId)
            .child(TOWNS_KEY_POINTLIST)
            .child(pointId)
            .child(TOWNS_KEY_DISCOVERED)
    usersRef.setValue(value)
}

fun setSharedPoint(dataId: String, pointId: String, value: String, context: Context) {
    val user = getCurrentUser(context)
    val usersRef = mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(TOWNS_KEY_TOWNS)
            .child(dataId)
            .child(TOWNS_KEY_POINTLIST)
            .child(pointId)
            .child(TOWNS_KEY_POINT_SHARED)
    usersRef.setValue(value)

    val usersRef2 = mDatabase
            .child(SHARED_KEY_SHARED)
            .child(dataId)
            .child(TOWNS_KEY_POINTLIST)
            .child(pointId)
            .child(TOWNS_KEY_POINT_SHARED)
    usersRef2.setValue(value)
}

fun getDiscoveredPointValue(dataId: String, pointId: String, context: Context): DatabaseReference {
    val user = getCurrentUser(context)
    return mDatabase
            .child(USER_KEY_USERS)
            .child(user)
            .child(TOWNS_KEY_TOWNS)
            .child(dataId)
            .child(TOWNS_KEY_POINTLIST)
            .child(pointId)
            .child(TOWNS_KEY_DISCOVERED)
}

fun setMainListData(context: MainView.IDataClicked, view: MainView) {
    val appointmentsQuery = getMainListQuery(mDatabase)

    val options = FirebaseRecyclerOptions.Builder<Data>()
            .setQuery(appointmentsQuery, Data::class.java)
            .build()

    view.setDataList(options, context)
}

fun setInfoListData(context: InfoView.IInfoClicked, view: InfoView, idTown: String) {
    val storesQuery = getInfoListQuery(mDatabase, idTown)

    val options = FirebaseRecyclerOptions.Builder<Info>()
            .setQuery(storesQuery, Info::class.java)
            .build()

    view.setInfoList(options, context)
}

fun setUsersListData(view: ShareView, pointData: Point?) {
    val usersQuery = getUserListQuery(mDatabase)

    val options = FirebaseRecyclerOptions.Builder<Data>()
            .setQuery(usersQuery, Data::class.java)
            .build()

    view.setUsersList(options, pointData)
}

fun uploadPointImage(pointId: String, dataId: String, context: Activity, imageUri: Uri?) {
    val progressDialog = ProgressDialog(context)
    progressDialog.setTitle("Uploading...")
    progressDialog.show()

    val user = getCurrentUser(context)
    val ref = mStorage
            .child(STORAGE_KEY_IMAGENES)
            .child(STORAGE_KEY_POINTS)
            .child(pointId)
            .child(user)

    ref.putFile(imageUri!!)
            .addOnSuccessListener { it ->
                progressDialog.dismiss()
                Toast.makeText(context, "Uploaded", Toast.LENGTH_SHORT).show()
                ref.downloadUrl.addOnSuccessListener {
                    setSharedPoint(dataId, pointId, it.toString(), context)
                    context.finish()
                }
            }
            .addOnFailureListener { e ->
                progressDialog.dismiss()
                Toast.makeText(context, "Failed " + e.message, Toast.LENGTH_SHORT).show()
            }
            .addOnProgressListener { taskSnapshot ->
                val progress = 100.0 * taskSnapshot.bytesTransferred / taskSnapshot.totalByteCount
                progressDialog.setMessage("Uploaded " + progress.toInt() + "%")
            }
}

private fun getMainListQuery(databaseReference: DatabaseReference): Query {
    return databaseReference.child(TOWNS_KEY_TOWNS)
}

private fun getInfoListQuery(databaseReference: DatabaseReference, idTown: String): Query {
    return databaseReference.child(STORES_KEY_STORES).child(idTown)
}

private fun getUserListQuery(databaseReference: DatabaseReference): Query {
    return databaseReference.child(SHARED_KEY_SHARED)
}