package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.mvp.view.MainView
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_DATA
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_LOGOUT
import com.example.felipetorres.townpaths.mvp.view.base.LoginActivity
import com.example.felipetorres.townpaths.mvp.view.base.MapActivity
import com.example.felipetorres.townpaths.mvp.view.base.setMainListData
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.facebook.login.LoginManager
import kotlinx.android.synthetic.main.activity_main.*

class MainPresenter(private val view: MainView) : MainView.IDataClicked {

    private val context: Activity?


    init {
        this.context = view.activity
        setMainListData(this, view)
        context?.logout?.setOnClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setMessage("¿Estas seguro que quieres salir de la aplicación?")
            builder.setCancelable(false)

            builder.setPositiveButton("SI") { dialog, _ ->
                dialog.cancel()
                val i = Intent(context, LoginActivity::class.java)
                i.putExtra(BUNDLE_KEY_LOGOUT, true)
                context.startActivity(i)
                context.finishAffinity()
                context.finish()
            }

            builder.setNegativeButton("NO") { dialog, _ -> dialog.cancel() }

            val alert = builder.create()
            alert.show()

        }
    }

    fun register() {
        val activity = view.activity ?: return
    }

    fun unregister() {
        val activity = view.activity ?: return

        RxBus.clear(activity)
    }

    fun onStart() {
        view.adapterStartListening()
    }

    fun onStop() {
        view.adapterStopListening()
    }

    override fun goToData(data: Data) {
        val i = Intent(context, MapActivity::class.java)
        i.putExtra(BUNDLE_KEY_DATA, data)
        context?.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        context?.startActivity(i)
    }
}
