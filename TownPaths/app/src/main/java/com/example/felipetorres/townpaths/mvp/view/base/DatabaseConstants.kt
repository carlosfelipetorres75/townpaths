package com.example.felipetorres.townpaths.mvp.view.base

//Firebase constants
const val USER_KEY_USERS = "users"
const val USER_KEY_SCORE = "score"
const val USER_KEY_AWARDS = "awards"
const val USER_KEY_SEEN_CONTENT = "seenContent"

const val TOWNS_KEY_TOWNS = "towns"
const val STORES_KEY_STORES = "stores"
const val TOWNS_KEY_POINTLIST = "pointlist"
const val TOWNS_KEY_DISCOVERED = "discovered"
const val TOWNS_KEY_POINT_SHARED = "pointShared"
const val SHARED_KEY_SHARED = "shared"

const val STORAGE_KEY_IMAGENES = "imagenes"
const val STORAGE_KEY_POINTS = "points"

//Bundle intent constants
const val BUNDLE_KEY_DATA = "data"
const val BUNDLE_KEY_INFO = "info"
const val BUNDLE_KEY_POINT_DATA = "pointData"
const val BUNDLE_KEY_CONTENT_TYPE = "typeContent"
const val BUNDLE_KEY_CONTENT = "content"
const val BUNDLE_KEY_POSITION = "position"
const val BUNDLE_KEY_PAGE = "page"
const val BUNDLE_KEY_NORMAL_MAP = "normalMap"
const val BUNDLE_KEY_USER = "user"
const val BUNDLE_KEY_AWARD = "award"
const val BUNDLE_KEY_LOGOUT = "logout"

//Settings constants
const val SETTINGS_KEY_USER = "user"
const val SETTINGS_KEY_IN_MOGUI = "inMongui"

//score Constants
const val SCORE_MIN = 10L
const val SCORE_MEDIUM = 50L
const val SCORE_MAX = 100L