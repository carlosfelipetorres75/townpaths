package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AppCompatActivity

class PagerContentView(activity: AppCompatActivity): ActivityView(activity){
    init {
        setTitleToolbar("Contenido")
    }
}
