package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.MapView
import com.example.felipetorres.townpaths.mvp.view.base.*
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MapStyleOptions
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_map.info_scores
import kotlinx.android.synthetic.main.activity_map.play_intro_video
import kotlinx.android.synthetic.main.activity_map.zoomin
import kotlinx.android.synthetic.main.activity_map.zoomout
import java.net.URL


class MapPresenter(private val view: MapView) : OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    private val context: Activity?
    private var mMap: GoogleMap? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var location: Location? = null
    private lateinit var mLocationRequest: LocationRequest
    private val settings: SharedPreferences

    private var data: Data? = null
    private var pointData: Point? = null
    private var normalMap: Boolean = false


    init {
        this.context = view.activity

        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences

        val mapFragment = context.supportFragmentManager?.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mGoogleApiClient = context.let {
            GoogleApiClient.Builder(it)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build()
        }

        context.info_scores.setOnClickListener {
            showInfoWindow()
        }

        val builder = AlertDialog.Builder(context)
        builder.setMessage("¿Actualmente te encuentras en Mongui?")
        builder.setCancelable(false)

        builder.setPositiveButton("SI") { dialog, _ ->
            settings.edit().putBoolean(SETTINGS_KEY_IN_MOGUI, true).apply()
            dialog.cancel()
        }

        builder.setNegativeButton("NO") { dialog, _ ->
            settings.edit().putBoolean(SETTINGS_KEY_IN_MOGUI, false).apply()
            dialog.cancel()
        }

        val alert = builder.create()
        alert.show()

    }

    fun register() {
        val activity = view.activity ?: return
    }

    fun unregister() {
        val activity = view.activity ?: return

        RxBus.clear(activity)
    }

    fun onStart() {
        mGoogleApiClient?.connect()
        mMap?.let { onMapReady(it) }
    }

    fun onStop() {
        mGoogleApiClient?.disconnect()
    }

    private fun zoomToLocation(lat: Double?, lng: Double?) {
        if (lat != null && lng != null) mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), 16f), 2000, null)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap?.clear()
        mMap?.setMaxZoomPreference(17f)
        mMap?.setMinZoomPreference(15f)

        mMap?.uiSettings?.setAllGesturesEnabled(false)
        mMap?.uiSettings?.isZoomGesturesEnabled = true
        mMap?.uiSettings?.isZoomControlsEnabled = false
        mMap?.uiSettings?.isMyLocationButtonEnabled = false
        mMap?.setOnMarkerClickListener(this)
        val density = context?.resources?.displayMetrics?.density ?: 1f

        if (!normalMap) {
            mMap?.setOnInfoWindowClickListener(this)
            mMap?.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.style_json))
            mMap?.isMyLocationEnabled = false
            mMap?.uiSettings?.isMapToolbarEnabled = false
        }

        zoomToLocation(data?.lat, data?.lng)

        if (pointData != null) {
            val name = "Punto por descubrir"
            val snippet = ""
            val bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.placeholder)

            context?.runOnUiThread {
                val puente = googleMap.addMarker(MarkerOptions().position(LatLng(pointData?.lat
                        ?: 0.0, pointData?.lng ?: 0.0))
                        .title(name)
                        .snippet(snippet)
                        .icon(bitmapDescriptor)
                        .visible(true)
                        .flat(false))
                puente.tag = pointData?.id
            }
        } else {
            data?.pointlist?.map { it ->
                var name = "Punto por descubrir"
                var snippet = "Haz click aquí para ver más"
                var bitmap: Bitmap
                val resizedBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(context?.resources, R.drawable.placeholder), (50 * density).toInt(), (50 * density).toInt(), false)
                var bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(resizedBitmap)

                context?.let { it1 ->
                    getDiscoveredPointValue(data?.id.toString(), it.value.id.toString(), it1)
                            .addValueEventListener(object : ValueEventListener {
                                override fun onCancelled(p0: DatabaseError) {}

                                override fun onDataChange(dataSnapshot: DataSnapshot) {
                                    val value = dataSnapshot.value
                                    Thread(Runnable {
                                        it.value.let {
                                            if (value != null && value == true) {
                                                try {
                                                    it.discovered = true
                                                    name = it.name.toString()
                                                    snippet = "Explora una vez más"
                                                    val url = URL(it.image)
                                                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
                                                    bitmapDescriptor = BitmapDescriptorFactory.fromBitmap(Bitmap.createScaledBitmap(bitmap, (50 * density).toInt(), (50 * density).toInt(), false))
                                                } catch (e: Exception) {
                                                    e.printStackTrace()
                                                }
                                            }
                                        }

                                        context.runOnUiThread {
                                            val puente = googleMap.addMarker(MarkerOptions().position(LatLng(it.value.lat
                                                    ?: 0.0, it.value.lng ?: 0.0))
                                                    .title(name)
                                                    .snippet(snippet)
                                                    .icon(bitmapDescriptor)
                                                    .visible(true)
                                                    .flat(false))
                                            puente.tag = it.value.id
                                        }
                                    }).start()
                                    if (value == null) {

                                    }
                                }

                            })
                }
            }
        }

        context?.zoomin?.setOnClickListener {
            mMap?.animateCamera(CameraUpdateFactory.zoomIn())
        }

        context?.zoomout?.setOnClickListener {
            mMap?.animateCamera(CameraUpdateFactory.zoomOut())
        }
    }

    override fun onMarkerClick(marker: Marker?): Boolean {
        if (normalMap) {
            Toast.makeText(context, "Utiliza el boton de navegacion en la esquina inferior derecha para guiarte hasta el punto", Toast.LENGTH_LONG).show()
        }
        return false
    }

    override fun onInfoWindowClick(marker: Marker?) {
        val i: Intent = if (data?.pointlist?.get(marker?.tag)?.discovered == true) {
            Intent(context, PagerContentActivity::class.java)
        } else {
            Intent(context, ClueActivity::class.java)
        }
        i.putExtra(BUNDLE_KEY_POINT_DATA, data?.pointlist?.get(marker?.tag))
        i.putExtra(BUNDLE_KEY_DATA, data)
        context?.startActivity(i)
    }

    override fun onConnected(p0: Bundle?) {
        mLocationRequest = LocationRequest.create()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 1000 // Update location every 1 seconds

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this)

        location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (location == null) {
            context?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            return
        }
    }

    override fun onConnectionSuspended(p0: Int) {
        Log.i(MapActivity::class.java.name, "GoogleApiClient connection has been suspend")
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Log.i(MapActivity::class.java.name, "GoogleApiClient connection has failed")
    }

    override fun onLocationChanged(p0: Location?) {
        Log.i(MapActivity::class.java.name, "Location received: " + location.toString())
    }

    fun getExtras(intent: Intent?) {
        data = intent?.getSerializableExtra(BUNDLE_KEY_DATA) as Data
        val pointDataVal = intent.getSerializableExtra(BUNDLE_KEY_POINT_DATA)
        if (pointDataVal != null) pointData = pointDataVal as Point
        normalMap = intent.getBooleanExtra(BUNDLE_KEY_NORMAL_MAP, false)

        getUserScore(data?.id.toString(), view)

        context?.play_intro_video?.setOnClickListener {
            showVideoDialog()
        }
    }

    private fun showVideoDialog() {
        val i = Intent(context, DialogActivity::class.java)
        i.putExtra(BUNDLE_KEY_DATA, data)
        context?.startActivity(i)
        context?.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }

    private fun showInfoWindow() {
        val i = Intent(context, InfoActivity::class.java)
        i.putExtra(BUNDLE_KEY_DATA, data)
        context?.startActivity(i)
        context?.overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
    }
}

