package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AppCompatActivity

class QuizView(activity: AppCompatActivity): ActivityView(activity){
    init {
        setTitleToolbar("Quiz")
    }
}
