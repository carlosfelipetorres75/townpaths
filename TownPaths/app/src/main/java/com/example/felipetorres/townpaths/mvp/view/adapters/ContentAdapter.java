package com.example.felipetorres.townpaths.mvp.view.adapters;


import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.felipetorres.townpaths.R;
import com.example.felipetorres.townpaths.models.Content;

import java.util.ArrayList;
import java.util.List;

public class ContentAdapter extends RecyclerView.Adapter<ContentAdapter.MyViewHolder> {

    private ArrayList<Content> contents;
    private ISelectedContent iSelectedContent;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        ImageView image;
        TextView description;
        CardView cardView;

        MyViewHolder(View view) {
            super(view);
            name = view.findViewById(R.id.tv_name);
            image = view.findViewById(R.id.iv_town);
            description = view.findViewById(R.id.tv_description);
            cardView = view.findViewById(R.id.cv_image_info);
        }
    }


    public ContentAdapter(List<Content> contentList, ISelectedContent iSelectedContent) {
        this.contents = new ArrayList<>(contentList);
        this.iSelectedContent = iSelectedContent;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_content, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Content content = contents.get(position);
        final int pos = position;
        if (content != null){
            holder.name.setText(content.getTitle());
            holder.description.setText(content.getDescription());

            Glide.with(holder.cardView).load(content.getImage()).into(holder.image);

            holder.cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    iSelectedContent.goToSelectedContent(content, pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return contents.size();
    }

    public interface ISelectedContent {
        void goToSelectedContent(Content content, int pos);
    }
}
