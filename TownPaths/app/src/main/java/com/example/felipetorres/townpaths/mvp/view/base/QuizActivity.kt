package com.example.felipetorres.townpaths.mvp.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.QuizPresenter
import com.example.felipetorres.townpaths.mvp.view.QuizView

class QuizActivity: AppCompatActivity() {

    private var presenter: QuizPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)
        presenter = QuizPresenter(QuizView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }
}
