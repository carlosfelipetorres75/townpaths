package com.example.felipetorres.townpaths.mvp.view.adapters

import androidx.recyclerview.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.example.felipetorres.townpaths.R


class UserViewHolder(val view: View): androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {

    private val image: ImageView = view.findViewById(R.id.iv_user)

    fun bindToUser(imageUrl: String) {
        Glide.with(view.context).load(imageUrl).into(image)
    }

}