package com.example.felipetorres.townpaths.mvp.view.base

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.SharePresenter
import com.example.felipetorres.townpaths.mvp.view.ShareView

class ShareActivity: AppCompatActivity() {

    private var presenter: SharePresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share)

        presenter = SharePresenter(ShareView(this))
        presenter?.getExtras(intent)

        val fotoButton = findViewById<Button>(R.id.foto)
        fotoButton.setOnClickListener {
            presenter?.takePhoto()
        }
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }

    public override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    public override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        presenter?.onActivityResult(requestCode, resultCode, data)
    }
}
