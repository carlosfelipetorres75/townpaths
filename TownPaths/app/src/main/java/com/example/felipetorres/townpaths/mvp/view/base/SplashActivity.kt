package com.example.felipetorres.townpaths.mvp.view.base

import android.Manifest
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.TargetApi
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import com.example.felipetorres.townpaths.R
import kotlinx.android.synthetic.main.activity_splash.iv_logo
import kotlinx.android.synthetic.main.activity_splash.progress_bar

class SplashActivity: AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        init()
    }

    /**
     * Initializes the application
     */
    private fun init() {
        iv_logo?.alpha = 0.0f
        Handler().postDelayed({ animateLogo() }, ANIM_DELAY.toLong())

        if (!validatePermissions()) {
            ActivityCompat.requestPermissions(this, INITIAL_PERMS, 101)
        } else {
            Handler().postDelayed({ redirectActivity() }, SPLASH_DELAY.toLong())
        }
    }

    private fun redirectActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finishAffinity()
        finish()
    }

    private fun animateLogo() {
        val scaleXAnimation = createObjectAnimator(iv_logo, "scaleX", 0.0f, 1.0f,
            ANIM_DURATION.toLong())
        val scaleYAnimation = createObjectAnimator(iv_logo, "scaleY", 0.0f, 1.0f,
            ANIM_DURATION.toLong())
        val alphaAnimation = createObjectAnimator(iv_logo, "alpha", 0.0f, 1.0f,
            ANIM_DURATION.toLong())
        val animatorSet = AnimatorSet()
        animatorSet.play(scaleXAnimation).with(scaleYAnimation).with(alphaAnimation)
        animatorSet.start()
    }

    private fun validatePermissions(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true
        }
        for (permission in INITIAL_PERMS) {
            if (!hasPermission(permission)) {
                return false
            }
        }
        return true
    }

    @TargetApi(Build.VERSION_CODES.M)
    private fun hasPermission(perm: String): Boolean {
        return PackageManager.PERMISSION_GRANTED == ContextCompat.checkSelfPermission(this, perm)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
        grantResults: IntArray) {
        when (requestCode) {
            101 -> {
                if (grantResults.size == 0 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.i("PERMISSIONS", "Permission has been denied by user")
                    finish()
                } else {
                    Log.i("PERMISSIONS", "Permission has been granted by user")
                    Handler().postDelayed({ redirectActivity() }, SPLASH_DELAY.toLong())
                }
            }
        }
    }

    fun toggleProgressBar() {
        if (progress_bar?.visibility == View.INVISIBLE) {
            progress_bar?.visibility = View.VISIBLE
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        } else {
            progress_bar?.visibility = View.INVISIBLE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    companion object {

        private val INITIAL_PERMS = arrayOf(Manifest.permission.INTERNET,
            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.VIBRATE, Manifest.permission.CAMERA)

        private val ANIM_DELAY = 500
        private val ANIM_DURATION = 1000
        private val SPLASH_DELAY = 2500

        fun createObjectAnimator(view: View?, property: String, init: Float,
            end: Float, duration: Long): ObjectAnimator {
            val scaleXAnimation = ObjectAnimator.ofFloat(view, property, init, end)
            scaleXAnimation.interpolator = AccelerateDecelerateInterpolator()
            scaleXAnimation.duration = duration
            return scaleXAnimation
        }
    }
}

