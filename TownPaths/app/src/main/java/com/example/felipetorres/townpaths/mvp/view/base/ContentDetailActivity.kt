package com.example.felipetorres.townpaths.mvp.view.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.ContentDetailPresenter
import com.example.felipetorres.townpaths.mvp.view.ContentDetailView

class ContentDetailActivity: AppCompatActivity() {

    private var presenter: ContentDetailPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_detail)
        presenter = ContentDetailPresenter(ContentDetailView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }
}
