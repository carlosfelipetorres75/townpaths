package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AlertDialog
import com.bumptech.glide.Glide
import com.example.felipetorres.townpaths.models.Award
import com.example.felipetorres.townpaths.models.Data
import com.example.felipetorres.townpaths.mvp.view.RedeemView
import com.example.felipetorres.townpaths.mvp.view.base.*
import com.example.felipetorres.townpaths.util.bus.RxBus
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_redeem.description
import kotlinx.android.synthetic.main.activity_redeem.howto
import kotlinx.android.synthetic.main.activity_redeem.image
import kotlinx.android.synthetic.main.activity_redeem.swipeBtn

class RedeemPresenter(val view: RedeemView) {

    private val context: Activity?
    private var data: Data? = null
    private var award: Award? = null

    init {
        this.context = view.activity

        context?.swipeBtn?.setOnStateChangeListener { active ->
            if (active) {
                val builder = AlertDialog.Builder(context)
                builder.setMessage("Estas seguro que quieres redimir este descuento?")
                builder.setCancelable(false)

                builder.setPositiveButton("SI") {
                    dialog, _ ->  dialog.cancel()
                    award?.howto?.let { setUserScore(data?.id.toString(), it.toLong(), context, false) }
                    award?.id?.let { setWonAwards(context, it) }
                    context.finish()
                }

                builder.setNegativeButton("NO"){
                    dialog, _ -> dialog.cancel()
                    context.swipeBtn.toggleState()
                }

                val alert = builder.create()
                alert.show()
            }
        }
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun getExtras(intent: Intent?) {
        data = intent?.getSerializableExtra(BUNDLE_KEY_DATA) as Data
        award = intent.getSerializableExtra(BUNDLE_KEY_AWARD) as Award
        getUserScore(data?.id.toString(), view)

        context?.let { Glide.with(it).load(award?.image).into(it.image) }
        context?.description?.text = award?.description
        context?.howto?.text = award?.howto.toString() + " puntos"
    }
}