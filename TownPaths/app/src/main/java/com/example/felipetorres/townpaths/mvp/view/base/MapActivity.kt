package com.example.felipetorres.townpaths.mvp.view.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.MapPresenter
import com.example.felipetorres.townpaths.mvp.view.MapView

class MapActivity: AppCompatActivity() {

    private var presenter: MapPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        presenter = MapPresenter(MapView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }

    public override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    public override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }
}
