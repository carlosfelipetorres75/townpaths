package com.example.felipetorres.townpaths.mvp.view.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.base.ContentFragment


class ContentPagerAdapter(fragmentManager: FragmentManager, val pointData: Point?): FragmentPagerAdapter(fragmentManager) {

    var NAME_PAGES = arrayListOf("Contenido", "Quiz")
    var NAME_PAGES_NARRATIVA = arrayListOf("Mini-documentales", "Historias")
    override fun getCount(): Int {
        return NUM_ITEMS
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> pointData?.let { ContentFragment.newInstance(it, 0) }!!
            else -> pointData?.let { ContentFragment.newInstance(it, 1) }!!

        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (pointData?.id == "point6") NAME_PAGES_NARRATIVA[position] else NAME_PAGES[position]
    }

    companion object {
        private val NUM_ITEMS = 2
    }

}