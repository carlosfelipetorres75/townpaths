package com.example.felipetorres.townpaths.models

import java.io.Serializable


class Content(
    var id: String? = null,
    var title: String? = null,
    var description: String? = null,
    var image: String? = null,
    var video: String? = null,
    var questions: ArrayList<Question>? = null): Serializable