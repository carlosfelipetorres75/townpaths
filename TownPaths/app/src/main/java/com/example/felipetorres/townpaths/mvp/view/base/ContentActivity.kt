package com.example.felipetorres.townpaths.mvp.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.ContentPresenter
import com.example.felipetorres.townpaths.mvp.view.ContentView

class ContentActivity: AppCompatActivity() {

    private var presenter: ContentPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        presenter = ContentPresenter(ContentView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
