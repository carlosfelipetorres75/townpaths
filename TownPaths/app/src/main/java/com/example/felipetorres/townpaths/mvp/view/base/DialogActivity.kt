package com.example.felipetorres.townpaths.mvp.view.base

import android.app.ProgressDialog
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View.VISIBLE
import android.widget.MediaController
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Data
import kotlinx.android.synthetic.main.activity_dialog.*

class DialogActivity : AppCompatActivity() {

    private var data: Data? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog)

        data = intent?.getSerializableExtra(BUNDLE_KEY_DATA) as Data

        val progressDialog = ProgressDialog(this)
        progressDialog.setTitle("Un Momento")
        progressDialog.setMessage("Cargando video de " + data?.name)

        video.setVideoURI(Uri.parse(data?.video))
        video.requestFocus()
        video.setOnPreparedListener {
            progressDialog.dismiss()
            val mediaControls = MediaController(this, true)
            //video.setMediaController(mediaControls)
            video.start()

            Thread {
                try {
                    val duration = video.duration
                    progress_bar.max = duration
                    do {
                        val time = (duration - video.currentPosition) / 1000
                        progress_bar.progress = video.currentPosition
                    } while (video.currentPosition < duration)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }.start()
        }
        video.setOnCompletionListener {
            finish()
        }
        video.setOnErrorListener { mediaPlayer, i, j ->
            mediaPlayer.stop()
            false
        }

        if (!this.isFinishing) {
            progressDialog.show()
        }
    }
}
