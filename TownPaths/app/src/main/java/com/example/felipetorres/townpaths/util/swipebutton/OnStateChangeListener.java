package com.example.felipetorres.townpaths.util.swipebutton;

public interface OnStateChangeListener {
    void onStateChange(boolean active);
}
