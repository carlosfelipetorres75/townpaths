package com.example.felipetorres.townpaths.util.bus.observers

import io.reactivex.functions.Consumer

abstract class BusObserver<T>(private val clazz: Class<T>): Consumer<Any> {

    @Throws(Exception::class)
    override fun accept(value: Any) {
        if (value.javaClass == clazz) {
            onEvent(value as T)
        }
    }

    abstract fun onEvent(value: T)
}
