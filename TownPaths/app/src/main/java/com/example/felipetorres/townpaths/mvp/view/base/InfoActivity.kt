package com.example.felipetorres.townpaths.mvp.view.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.InfoPresenter
import com.example.felipetorres.townpaths.mvp.view.InfoView

class InfoActivity: AppCompatActivity() {

    private var presenter: InfoPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        presenter = InfoPresenter(InfoView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }

    public override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    public override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }
}
