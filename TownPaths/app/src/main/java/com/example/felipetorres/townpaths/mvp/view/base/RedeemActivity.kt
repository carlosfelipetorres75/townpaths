package com.example.felipetorres.townpaths.mvp.view.base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.RedeemPresenter
import com.example.felipetorres.townpaths.mvp.view.RedeemView

class RedeemActivity: AppCompatActivity() {

    private var presenter: RedeemPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redeem)

        presenter = RedeemPresenter(RedeemView(this))
        presenter?.getExtras(intent)
    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }
}
