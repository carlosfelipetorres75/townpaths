package com.example.felipetorres.townpaths.models

import java.io.Serializable


class Award(
    var id: String? = null,
    var description: String? = null,
    var howto: Long? = null,
    var image: String? = null): Serializable