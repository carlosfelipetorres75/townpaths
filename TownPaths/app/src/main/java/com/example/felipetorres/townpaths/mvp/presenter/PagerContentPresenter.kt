package com.example.felipetorres.townpaths.mvp.presenter

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.mvp.view.PagerContentView
import com.example.felipetorres.townpaths.mvp.view.adapters.ContentPagerAdapter
import com.example.felipetorres.townpaths.mvp.view.base.BUNDLE_KEY_POINT_DATA
import com.example.felipetorres.townpaths.mvp.view.base.ShareActivity
import com.example.felipetorres.townpaths.mvp.view.base.getUserScore
import com.example.felipetorres.townpaths.util.bus.RxBus
import kotlinx.android.synthetic.main.activity_pager_content.share_card
import kotlinx.android.synthetic.main.activity_pager_content.tabLayout
import kotlinx.android.synthetic.main.activity_pager_content.viewpager

class PagerContentPresenter(val view: PagerContentView) {
    private val context: AppCompatActivity? = view.activity
    private var pointData: Point? = null
    private val settings: SharedPreferences

    init {
        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences

        context.share_card.setOnClickListener {
            val i = Intent(context, ShareActivity::class.java)
            i.putExtra(BUNDLE_KEY_POINT_DATA, pointData)
            context.startActivity(i)
        }
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun getExtras(intent: Intent?) {
        pointData = intent?.getSerializableExtra(BUNDLE_KEY_POINT_DATA) as Point
        context?.let { it ->
            it.viewpager?.adapter = context.supportFragmentManager?.let {
                ContentPagerAdapter(it, pointData)
            }
            it.tabLayout?.setupWithViewPager(context.viewpager)
        }
        getUserScore(pointData?.parent.toString(), view)
    }
}