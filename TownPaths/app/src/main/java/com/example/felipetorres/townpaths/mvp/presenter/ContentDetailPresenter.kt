package com.example.felipetorres.townpaths.mvp.presenter

import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.MediaController
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.models.Content
import com.example.felipetorres.townpaths.models.Point
import com.example.felipetorres.townpaths.models.Question
import com.example.felipetorres.townpaths.mvp.view.ContentDetailView
import com.example.felipetorres.townpaths.mvp.view.base.*
import com.example.felipetorres.townpaths.util.bus.RxBus
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_content.video
import kotlinx.android.synthetic.main.activity_content_detail.*


class ContentDetailPresenter(val view: ContentDetailView) {
    private val context: Activity?
    private var mediaControls: MediaController? = null
    private var position = 0
    private var progressDialog: ProgressDialog? = null
    private var content: Content? = null
    private var pointData: Point? = null
    private var typeContent: String? = null
    private var tries = 0
    private var videoCount = 1
    private val settings: SharedPreferences

    init {
        this.context = view.activity
        settings = context?.getSharedPreferences(context.getString(R.string.prefs), 0) as SharedPreferences
    }

    fun register() {
        val activity = context ?: return
    }

    fun unregister() {
        val activity = context ?: return

        RxBus.clear(activity)
    }

    fun getExtras(intent: Intent?) {
        //content = intent?.getSerializableExtra(BUNDLE_KEY_CONTENT) as Content
        typeContent = intent?.getSerializableExtra(BUNDLE_KEY_CONTENT_TYPE) as String
        pointData = intent.getSerializableExtra(BUNDLE_KEY_POINT_DATA) as Point
        val pos = intent.getIntExtra(BUNDLE_KEY_POSITION, 0)
        videoCount = pos + 1
        content = if (typeContent == "content") pointData?.content?.get(videoCount) else pointData?.narrative?.get(videoCount)
        setVideoSettings()
        //context?.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
    }

    private fun setVideoSettings() {
        if (mediaControls == null) {
            mediaControls = MediaController(context)
        }

        progressDialog = ProgressDialog(context)
        progressDialog?.setTitle("Un Momento")
        progressDialog?.setMessage("Cargando video de " + content?.title)
        progressDialog?.setCancelable(false)
        progressDialog?.show()

        //context?.video?.setMediaController(mediaControls)
        context?.video?.setVideoURI(Uri.parse(content?.video))
        context?.video?.requestFocus()
        context?.video?.setOnPreparedListener {
            progressDialog?.dismiss()
            context.video?.seekTo(position)
            if (position == 0) {
                context.video?.start()
            } else {
                context.video?.pause()
            }
            Thread {
                try {
                    val duration = context.video.duration
                    context.progress_bar.max = duration
                    do {
                        context.progress_bar.progress = context.video.currentPosition
                    } while (context.video.currentPosition < duration)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }.start()
        }
        context?.video?.setOnCompletionListener {
            setUserScore(pointData?.parent.toString(), SCORE_MEDIUM, context, true, content?.id) {
                Snackbar.make(context.video, if (it) "Puntos acumulados" else "No puedes acumular dos veces", Snackbar.LENGTH_LONG).show()
            }

            if (content?.questions != null) {
                Handler().postDelayed({
                    showQuizDialog()
                }, 1000)
            } else {
                try {
                    videoCount++
                    content = if (typeContent == "content") pointData?.content?.get(videoCount) else pointData?.narrative?.get(videoCount)
                    setVideoSettings()
                } catch (e: Exception) {
                    Handler().postDelayed({
                        this.view.activity?.finish()
                    },1000)
                }
            }
        }
        context?.video?.setOnErrorListener { mediaPlayer, i, j ->
            mediaPlayer.stop()
            false
        }
    }

    private fun showQuizDialog() {
        val dialog = Dialog(context!!)
        dialog.setContentView(R.layout.custom_quiz_dialog)
        dialog.setCancelable(false)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        val message = dialog.findViewById<TextView>(R.id.pointsToWin)
        val question = content?.questions?.filterNotNull()?.get(tries) as Question
        dialog.findViewById<TextView>(R.id.cluetext).text = question.question
        dialog.findViewById<TextView>(R.id.ans1).text = question.ans1
        dialog.findViewById<TextView>(R.id.ans2).text = question.ans2
        dialog.findViewById<TextView>(R.id.ans3).text = question.ans3

        val continueButton = dialog.findViewById<TextView>(R.id.btn_continue)
        val dialogButton = dialog.findViewById<TextView>(R.id.btn_dismiss)
        dialogButton.text = "Responder"
        dialogButton.setOnClickListener { view ->
            val selectedId = dialog.findViewById<RadioGroup>(R.id.radioAnswers).checkedRadioButtonId
            val radioButton = dialog.findViewById<RadioButton>(selectedId)
            val radioText = radioButton?.tag.toString()
            if (radioText == question.correct) {
                dialogButton.visibility = View.GONE
                continueButton.visibility = View.VISIBLE
                message.text = "Respuesta correcta!"

                setUserScore(pointData?.parent.toString(), SCORE_MEDIUM, context, true, question.id) {
                    Snackbar.make(view, if (it) "Puntos acumulados" else "No puedes acumular dos veces", Snackbar.LENGTH_LONG).show()
                }
                tries++
            } else {
                dialogButton.visibility = View.GONE
                continueButton.visibility = View.VISIBLE
                val correctText = when(question.correct?.takeLast(1)) {
                    "1" -> question.ans1
                    "2" -> question.ans2
                    else -> question.ans3
                }
                message.text = "Incorrecto, la respuesta es \"$correctText\""
            }
            tries++
        }

        continueButton.setOnClickListener {
            if (tries >= 0 && tries < content?.questions?.filterNotNull()?.size!!) {
                dialog.dismiss()
                showQuizDialog()
            } else {
                dialog.dismiss()
                //this.view.activity?.finish()
                videoCount++
                try {
                    content = if (typeContent == "content") pointData?.content?.get(videoCount) else pointData?.narrative?.get(videoCount)
                    setVideoSettings()
                } catch (e: Exception) {
                    this.view.activity?.finish()
                }
            }
        }
        if (!view.activity?.isFinishing!!) {
            dialog.show()
        }
    }
}