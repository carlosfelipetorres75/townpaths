package com.example.felipetorres.townpaths.mvp.view.adapters;


import android.content.Context;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.felipetorres.townpaths.R;
import com.example.felipetorres.townpaths.models.Award;

import java.util.ArrayList;
import java.util.List;

public class AwardsAdapter extends RecyclerView.Adapter<AwardsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Award> awards;
    private IAwardSelected iAwardSelected;
    private Long points;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView description;
        ImageView image;
        TextView howto;
        CardView cardView;
        RelativeLayout overlay;

        MyViewHolder(View view) {
            super(view);
            description = view.findViewById(R.id.description);
            image = view.findViewById(R.id.image);
            howto = view.findViewById(R.id.howto);
            cardView = view.findViewById(R.id.cv_image_award);
            overlay = view.findViewById(R.id.overlay);
        }
    }

    public AwardsAdapter(Context context, List<Award> awardList, IAwardSelected listener, Long points) {
        this.awards = new ArrayList<>(awardList);
        this.iAwardSelected = listener;
        this.points = points;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_award, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Award award = awards.get(position);
        if (award != null){
            holder.description.setText(award.getDescription());
            holder.howto.setText(award.getHowto() + " puntos");

            if (award.getHowto() <= points) {
                holder.overlay.setVisibility(View.GONE);
                holder.cardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        iAwardSelected.onAwardSelected(award);
                    }
                });
            }

            Glide.with(holder.image).load(award.getImage()).into(holder.image);
        }
    }

    @Override
    public int getItemCount() {
        return awards.size();
    }

    public interface IAwardSelected {
        void onAwardSelected(Award award);
    }
}
