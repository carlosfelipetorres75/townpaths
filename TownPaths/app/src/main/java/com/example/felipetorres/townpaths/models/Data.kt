package com.example.felipetorres.townpaths.models

import java.io.Serializable


class Data(
    var id: String? = null,
    var name: String? = null,
    var available: Boolean? = false,
    var imagen: String? = null,
    var video: String? = null,
    var lat: Double? = 0.0,
    var lng: Double? = 0.0,
    var pointlist: HashMap<String, Point>? = null): Serializable