package com.example.felipetorres.townpaths.mvp.view.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.felipetorres.townpaths.R
import com.example.felipetorres.townpaths.mvp.presenter.MainPresenter
import com.example.felipetorres.townpaths.mvp.view.MainView

class MainActivity: AppCompatActivity() {

    private var presenter: MainPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = MainPresenter(MainView(this))

    }

    override fun onResume() {
        super.onResume()
        presenter?.register()
    }

    override fun onPause() {
        super.onPause()
        presenter?.unregister()
    }

    public override fun onStart() {
        super.onStart()
        presenter?.onStart()
    }

    public override fun onStop() {
        super.onStop()
        presenter?.onStop()
    }
}
