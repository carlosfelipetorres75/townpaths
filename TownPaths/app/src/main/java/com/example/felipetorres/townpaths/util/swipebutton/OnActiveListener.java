package com.example.felipetorres.townpaths.util.swipebutton;

public interface OnActiveListener {
    void onActive();
}
