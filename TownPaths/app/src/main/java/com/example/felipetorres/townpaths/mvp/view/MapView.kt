package com.example.felipetorres.townpaths.mvp.view

import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MapView(activity: AppCompatActivity) : ActivityView(activity) {
    init {
        setTitleToolbar("Mapa")
    }
}
