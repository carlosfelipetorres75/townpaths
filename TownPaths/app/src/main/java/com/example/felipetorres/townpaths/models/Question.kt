package com.example.felipetorres.townpaths.models

import java.io.Serializable


class Question(
    var id: String? = null,
    var question: String? = null,
    var ans1: String? = null,
    var ans2: String? = null,
    var ans3: String? = null,
    var correct: String? = null): Serializable